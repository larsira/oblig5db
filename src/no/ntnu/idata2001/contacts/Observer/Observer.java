package no.ntnu.idata2001.contacts.Observer;

import java.util.Collection;
import no.ntnu.idata2001.contacts.model.AddressBook;
import no.ntnu.idata2001.contacts.model.ContactDetails;

/**
 * Represents observers, and is an interface for observers.
 *
 */
public interface Observer {
  
  /**
   * update to be executed whenever an observed object changes their state.
   * @param details new details
   */
  void update(Collection<ContactDetails> details);
}