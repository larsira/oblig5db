package no.ntnu.idata2001.contacts.model;

import java.io.Serializable;
import java.util.Collection;
import java.util.Iterator;
import java.util.logging.Logger;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.Query;
import no.ntnu.idata2001.contacts.Observer.Observer;

public class AddressBookDBHandler implements AddressBook, Serializable {
  private static final String PERSISTENCE_UNIT_NAME = "contacts-pu";
  private transient EntityManagerFactory efact;
  private transient EntityManager em;
  private transient Observer observer;
  private transient static final Logger logger = Logger.getLogger(AddressBookDBHandler.class.getName());
  
  /**
   * Default constructor
   */
  public AddressBookDBHandler() {
    this.efact = Persistence.createEntityManagerFactory(PERSISTENCE_UNIT_NAME);
  }
  
  /**
   * Adds a new ContactDetails object to the database.
   * @param contact object to add
   */
  @Override
  public void addContact(ContactDetails contact) {
    em = getEm();
    try{
     em.getTransaction().begin();
     if(this.findDetails(contact.getPhone())!=null) {
        logger.warning("Contact with this number already exists. Overwriting");
        this.updateContact(contact, contact.getPhone());
      } else {
       em.persist(contact);
       em.getTransaction().commit();
     }
    } finally {
      this.notifyObserver();
      this.close();
    }
  }
  
  /**
   * Removes the contact belonging to the specified phone number, if such a contact is present.
   * @param phoneNumber String matching the phone number of the contact to delete
   */
  @Override
  public void removeContact(String phoneNumber) {
    em = this.getEm();
    try{
      em.getTransaction().begin();
      ContactDetails contactDetails = em.merge(this.findDetails(phoneNumber));
      em.remove(contactDetails);
      em.getTransaction().commit();
      this.notifyObserver();
    } finally {
      this.close();
    }
  }
  
  /**
   * Finds a ContactDetails object based on its associated phone number.
   * @param phoneNumber String matching the phone number belonging to the contact to be found.
   * @return a contactDetails matching the phone number
   */
  private ContactDetails findDetails(String phoneNumber) {
    return em.find(ContactDetails.class, phoneNumber);
  }
  
  /**
   * Returns a collection of all ContactDetails in the database.
   * @return a collection of all ContactDetails in the database
   */
  @Override
  public Collection<ContactDetails> getAllContacts() {
    em = this.getEm();
    Query q = em.createQuery("SELECT OBJECT(o) FROM ContactDetails o");
    try {
      return q.getResultList();
    } finally {
      this.close();
    }
  }
  
  /**
   * Returns an iterator of all ContactDetails in the database
   * @return an iterator of all ContactDetails in the database
   */
  @Override
  public Iterator<ContactDetails> iterator() {
    return this.getAllContacts().iterator();
  }
  
  /**
   * Closes the specified EntityManager.
   */
  @Override
  public void close() {
    if(em.isOpen()) {
      em.close();
    }
  }
  
  /**
   * Updates the details of a contact, or removes it if its key changes.
   * @param contactDetails object to edit
   * @param oldKey old key for the object.
   */
  @Override
  public void updateContact(ContactDetails contactDetails, String oldKey) {
    this.removeContact(oldKey);
    em = this.getEm();
    try{
      em.getTransaction().begin();
      em.merge(contactDetails);
      em.getTransaction().commit();
      this.notifyObserver();
    } finally {
      this.close();
    }
  }
  
  /**
   * Creates an <code>EntityManager</code> and returns it.
   * @return new <code>EntityManager</code>
   */
  private EntityManager getEm() {
    return this.efact.createEntityManager();
  }
  
  /**
   * Sets the observer of this object.
   * @param observer object that will observe this object
   */
  @Override
  public void addObserver(Observer observer) {
    this.observer = observer;
    if(this.efact == null){
      this.efact = Persistence.createEntityManagerFactory(PERSISTENCE_UNIT_NAME);
    }
    notifyObserver();
  }
  
  /**
   * Notifies the observer of this object.
   */
  @Override
  public void notifyObserver() {
    observer.update(this.getAllContacts());
  }
}
