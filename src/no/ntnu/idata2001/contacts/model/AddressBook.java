package no.ntnu.idata2001.contacts.model;

import java.io.Serializable;
import java.util.Collection;
import java.util.Iterator;
import no.ntnu.idata2001.contacts.Observer.Observer;

public interface AddressBook extends Serializable, Iterable<ContactDetails> {
  void addContact(ContactDetails contact);
  
  void removeContact(String phoneNumber);
  
  void addObserver(Observer observer);
  
  void notifyObserver();
  
  Collection<ContactDetails> getAllContacts();
  
  @Override
  Iterator<ContactDetails> iterator();
  
  void close();
  
  void updateContact(ContactDetails contactDetails, String oldKey);
}
